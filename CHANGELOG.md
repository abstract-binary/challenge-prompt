0.4.0
=====

This release enables binary stripping in the cargo config which
reduces the executable size from 401KB to 307KB.  The minimum Rust
version is now 1.60.

### BREAKING CHANGES:
  - None

### New features:
  - None
  
### Changes:
  - Add `strip` and `opt-level` to cargo release config.


0.3.0
=====

This release adds the `--retries` flag to allow users to try solving
the challenge multiple times.

### BREAKING CHANGES:
  - The `-p` option no longer accepts an argument. Use `--phrase` for
    that

### New features:
  - Add `--retries` to allow users to retry answering the prompt
  - Add [nix flake](https://nixos.wiki/wiki/Flakes) configuration

### Changes:
  - Switch to argh parsing (exe size: 291K -> 295K)
  - The long version of `-p` is now `--mistake` option for the "I am
    probably making a mistake." prompt
  - Update rust edition to 2021
  - Update dependencies to latest
  - Use `argh` for parsing.

### Bugfixes:
  - None


0.2.0
=====

Add support for "yes" prompts.

0.1.0
=====

Initial release.
