use std::fmt;

#[derive(Debug)]
pub enum RngError {
    SystemTimeError(std::time::SystemTimeError),
    EnvVarMissing(String),
    EnvVarParseError {
        var_name: String,
        var_value: String,
        error: <u32 as std::str::FromStr>::Err,
    },
}

impl fmt::Display for RngError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for RngError {}

/// A basic `xorshift` RNG. See
/// [Wikipedia](https://en.wikipedia.org/wiki/Xorshift) for more details.
pub struct Rng(u32);

impl Rng {
    /// Create an `Rng` with the given seed.
    pub fn new(seed: u32) -> Rng {
        Rng(seed)
    }

    /// Create an `Rng` seeded from the current system time.
    pub fn new_from_time() -> Result<Rng, RngError> {
        std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .map(|d| Rng(d.subsec_millis()))
            .map_err(RngError::SystemTimeError)
    }

    /// Create an `Rng` seeded from an environment variable.
    pub fn new_from_env_var(var_name: &str) -> Result<Rng, RngError> {
        let value = std::env::vars()
            .find_map(|(key, value)| if key == var_name { Some(value) } else { None })
            .ok_or_else(|| RngError::EnvVarMissing(var_name.into()))?;
        value
            .parse::<u32>()
            .map(Rng)
            .map_err(|error| RngError::EnvVarParseError {
                var_name: var_name.into(),
                var_value: value,
                error,
            })
    }

    /// Generate a fresh `u32`.
    ///
    /// ```
    /// extern crate challenge_prompt;
    /// let mut rng = challenge_prompt::Rng::new(100);
    /// assert_eq!(rng.u32(), 27036706);
    /// assert_eq!(rng.u32(), 2466775534);
    ///
    /// ```
    pub fn u32(&mut self) -> u32 {
        let mut x = self.0;
        x ^= x << 13;
        x ^= x >> 17;
        x ^= x << 5;
        self.0 = x;
        x
    }
}
