use argh::FromArgs;
use challenge_prompt::*;
use std::alloc::System;
use std::process;

#[global_allocator]
static GLOBAL: System = System;

#[derive(Debug)]
enum Args {
    Version,
    Challenge {
        challenge: Challenge,
        retries: usize,
    },
}

fn main() {
    use Args::*;
    let args_raw: ArgsRaw = argh::from_env();
    match args_raw.try_into() {
        Ok(Version) => print_version(),
        Ok(Challenge { challenge, retries }) => {
            let mut rng = Rng::new_from_env_var("CHALLENGE_PROMPT_SEED").unwrap_or_else(|err1| {
                Rng::new_from_time().unwrap_or_else(|err2| {
                    eprintln!("Error: Failed to initialize RNG: {:?}, {:?}", err1, err2);
                    process::exit(1)
                })
            });
            for _ in 0..retries {
                if challenge.prompt(&mut rng) {
                    process::exit(0);
                }
            }
            process::exit(1);
        }
        Err(error) => {
            eprintln!("Error: {}", error);
            process::exit(1);
        }
    }
}

fn executable_name() -> String {
    std::env::args()
        .next()
        .unwrap_or_else(|| "challenge-prompt".into())
}

fn print_version() {
    println!("{} {}", executable_name(), env!("CARGO_PKG_VERSION"));
}

#[derive(FromArgs, Debug)]
#[argh(
    description = "Make the user pause before doing something dangerous.",
    note = "Set environment variable CHALLENGE_PROMPT_SEED to an u32 to fix the RNG seed.",
    error_code(0, "User passed the challenge."),
    error_code(1, "User failed the challenge or an error was encountered.")
)]
struct ArgsRaw {
    #[argh(switch, short = 'a')]
    /// prompt the user to solve a simple arithmetic problem (default challenge)
    arithmetic: bool,

    #[argh(switch, short = 'p')]
    /// prompt the user to type in "I am probably making a mistake."
    mistake: bool,

    #[argh(option)]
    /// prompt the user to type in exactly the given phrase
    phrase: Option<String>,

    #[argh(switch, short = 'y')]
    /// prompt the user to type in 'y' or 'yes'
    yes: bool,

    #[argh(switch, short = 'v')]
    /// print version information
    version: bool,

    #[argh(option, default = "1")]
    /// how many retries to allow before failing (default: 1)
    retries: usize,
}

impl TryFrom<ArgsRaw> for Args {
    type Error = String;

    fn try_from(args: ArgsRaw) -> Result<Self, Self::Error> {
        match (
            args.arithmetic,
            args.mistake || args.phrase.is_some(),
            args.yes,
        ) {
            (true, true, _) | (true, _, true) | (_, true, true) => {
                return Err("Multiple challenges selected".into())
            }
            (_, _, _) => (),
        }
        if args.version {
            Ok(Args::Version)
        } else if args.yes {
            Ok(Args::Challenge {
                challenge: Challenge::Yes,
                retries: args.retries,
            })
        } else if args.mistake {
            Ok(Args::Challenge {
                challenge: Challenge::Phrase(challenge_prompt::DEFAULT_PHRASE.into()),
                retries: args.retries,
            })
        } else if let Some(phrase) = args.phrase {
            Ok(Args::Challenge {
                challenge: Challenge::Phrase(phrase),
                retries: args.retries,
            })
        } else {
            Ok(Args::Challenge {
                challenge: Challenge::Arithmetic,
                retries: args.retries,
            })
        }
    }
}
