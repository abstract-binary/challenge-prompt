use assert_cmd::Command;
use predicates::prelude::*;

#[test]
fn arithmetic_challenge() {
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .assert()
        .stdout(predicate::str::contains("Solve:").from_utf8())
        .code(1);
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .arg("-a")
        .assert()
        .stdout(predicate::str::contains("Solve:").from_utf8())
        .code(1);
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .arg("--arithmetic")
        .assert()
        .stdout(predicate::str::contains("Solve:").from_utf8())
        .code(1);
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .write_stdin("100") // No prompt ever returns 100
        .assert()
        .code(1);
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .env("CHALLENGE_PROMPT_SEED", "100")
        .write_stdin("4")
        .assert()
        .code(0);
}

#[test]
fn phrase_challenge() {
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .arg("-p")
        .write_stdin(challenge_prompt::DEFAULT_PHRASE)
        .assert()
        .stdout(predicate::str::contains(challenge_prompt::DEFAULT_PHRASE).from_utf8())
        .code(0);
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .arg("--mistake")
        .write_stdin(challenge_prompt::DEFAULT_PHRASE)
        .assert()
        .stdout(predicate::str::contains(challenge_prompt::DEFAULT_PHRASE).from_utf8())
        .code(0);
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .arg("-p")
        .write_stdin("This is the wrong phrase.\n")
        .assert()
        .code(1);
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .arg("--mistake")
        .write_stdin("This is the wrong phrase.\n")
        .assert()
        .code(1);
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .arg("--phrase")
        .arg("To be or not to be")
        .write_stdin("  To be or not to be \n")
        .assert()
        .code(0);
}

#[test]
fn yes_no_challenge() {
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .arg("-y")
        .assert()
        .stdout(predicate::str::contains("[y]").from_utf8())
        .code(1);
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .arg("-y")
        .write_stdin("y")
        .assert()
        .code(0);
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .arg("-y")
        .write_stdin("yes")
        .assert()
        .code(0);
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .arg("--yes")
        .write_stdin("n")
        .assert()
        .code(1);
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .arg("--yes")
        .write_stdin("nooooo")
        .assert()
        .code(1);
}

#[test]
fn retry_challenge() {
    Command::cargo_bin("challenge-prompt")
        .unwrap()
        .args(["--yes", "--retries", "2"])
        .write_stdin("nooooo")
        .write_stdin("yes")
        .assert()
        .code(0);
}
